# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Bool, Eval
from trytond.transaction import Transaction


class TaxCodeTemplate(metaclass=PoolMeta):
    __name__ = 'account.tax.code.template'

    aeat_report = fields.Selection([
            (None, ''),
            ('111', "Model 111"),
            ('115', "Model 115"),
            ('303', "Model 303"),
            ], "AEAT Report")

    def _get_tax_code_value(self, code=None):
        value = super(TaxCodeTemplate, self)._get_tax_code_value(code=code)
        value['aeat_report'] = self.aeat_report
        return value


class TaxCode(metaclass=PoolMeta):
    __name__ = 'account.tax.code'

    aeat_report = fields.Selection([
            (None, ''),
            ('111', "Model 111"),
            ('115', "Model 115"),
            ('303', "Model 303"),
            ], "AEAT Report",
        states={
            'readonly': (Bool(Eval('template', -1))
                & ~Eval('template_override', False)),
            })


class PrintTaxesByInvoiceAndPeriodStart(metaclass=PoolMeta):
    __name__ = 'account_jasper_reports.print_taxes_by_invoice.start'

    retention_tax = fields.Boolean('Retention Taxes',
        states={
            'invisible': (Eval('partner_type') != 'suppliers')
        })

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.taxes.states['invisible'] = Eval('retention_tax')

    @fields.depends('retention_tax')
    def on_change_retention_tax(self):
        self.taxes = []


class PrintTaxesByInvoiceAndPeriod(metaclass=PoolMeta):
    __name__ = 'account_jasper_reports.print_taxes_by_invoice'

    def do_print_(self, action):
        action, data = super().do_print_(action)
        data['irpf'] = getattr(self, 'retention_tax', False)
        return action, data


class TaxesByInvoiceReport(metaclass=PoolMeta):
    __name__ = 'account_jasper_reports.taxes_by_invoice'

    @classmethod
    def _get_invoice_tax_domain(cls, data):
        domain = super()._get_invoice_tax_domain(data)
        if data.get('irpf', False):
            TaxCode = Pool().get('account.tax.code')

            tax_codes = TaxCode.search([
                ('company', '=', Transaction().context.get('company')),
                ('aeat_report', 'in', ['111', '115'])])
            taxes = []
            for tax_code in tax_codes:
                for line in tax_code.lines:
                    taxes.append(line.tax.id)
            domain.append(('tax.id', 'in', list(set(taxes))))

        return domain
